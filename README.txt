CONTENTS OF THIS FILE
---------------------
1) Introduction
2) Requirements
3) Installation
4) Configuration

INTRODUCTION
------------

Reverse Proxy Check is a simple module that adds a check to the Status Report to show 
whether or not the home page of your site is being served by a reverse proxy cache
server for anonymous users.

REQUIREMENTS
------------

This module not depandent with other module.

 INSTALLATION
 ------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further informations.

 CONFIGURATION
 -------------

 * We don't have any configuration step. When the module is installed configuration
   sone automatically.